import React, { Component } from 'react';
import { render } from 'react-dom';
import './styles.css';
import App from './todoListComponent/app';

render(<App />, document.getElementById('app'));
