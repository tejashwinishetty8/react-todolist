import React from 'react';

export default class TodoList extends React.Component {
    render() {
        
      return (
        <form onSubmit = {this.handleCreate.bind(this)}>
            <input type="text" data="whatever" ref="createInput"/>
            <button className="submit">submit</button>    
        </form>
      );
    }
    handleCreate(event){
        event.preventDefault();
        this.props.createTask(this.refs.createInput.value);
        this.refs.createInput.value='';
    }
  }
  