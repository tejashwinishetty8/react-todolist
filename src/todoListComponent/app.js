import React from 'react';
import TodosList from './todo-List';
import Items from './items';

const todos = [];

 class app extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      todos
    };
  } 
  render() {
      return (
        <div className="container">
         <h1>Todo List</h1>
         <Items  createTask={this.createTask.bind(this)} />
         <TodosList todos={this.state.todos}
          toggleTask={this.toggleTask.bind(this)}
          deleteTask={this.deleteTask.bind(this)}
          taskStatus = {this.taskStatus.bind(this)}
          /> 
         </div>
      );
    }
    toggleTask(task){
      const found =  _.find(this.state.todos, todo => todo.task === task)
      found.status=!found.status;
      this.setState({todos: this.state.todos})
    }
    createTask(task){
      this.state.todos.push({
        task,
        status: false
      });
      this.setState({todos: this.state.todos});
      console.log(todos);  
    }
    deleteTask(id){
      var array = [...this.state.todos]; 
     for(let each in array)
     {
      if(array[each].status!=true){
       if(array[each].task===id)
         array.splice(each,1);  
      }
     }
     this.setState({todos: array});
    } 
    taskStatus(state)
    {
      var array = [...this.state.todos]; 
      for(let each in array)
      {
        if(array[each].task===state) {
          if(array[each].status!=true){
        array[each].status=true; 
          }
          else
          array[each].status=false; 
        }
      }
      this.setState({todos: array});
      console.log(todos);
    }  
  }
  export default app;