import React from 'react';
import load from 'lodash';
import TodosListItem from './todoListItem';


export default class todoList extends React.Component {
    renderItems(){
        const props = load.omit(this.props,)
        
        return load.map(this.props.todos, (todo, index) => <TodosListItem key={index}{...todo} {...this.props}/>)
    }
    
    render() {        
      return (
        <ul>
                {this.renderItems()}
           </ul>
      );
    }
  }
  