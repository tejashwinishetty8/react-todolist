import React from 'react';
 class TodoListItem extends React.Component {
    
    constructor(props){
        super(props);
        let count=0;
    }
    
    render() {  
        const {task, status} = this.props;
      return (
            
            <li id={this.props.task}>
            <input type="checkbox" id="myCheck" onClick={this.checkStatus.bind(this)}/>
            <p> {this.props.task}</p>
            <button className="delete" onClick={this.deleteItem.bind(this)}>X</button></li>
      );
    }
    deleteItem(event){
        event.preventDefault();
        console.log("value  "+event.target.parentNode.id);      
        this.props.deleteTask(event.target.parentNode.id);
    }
    checkStatus(event){
        this.props.taskStatus(event.target.parentNode.id);  
    }
}
export default TodoListItem;